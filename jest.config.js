module.exports = {
    preset: 'ts-jest',
    globals: {},
    testEnvironment: 'jsdom',
    transform: {
        '^.+\\.vue$': 'vue3-jest',
        '^.+\\.(ts|tsx)?$': 'ts-jest',
        '^.+\\js$': 'babel-jest',
    },
    watchPathIgnorePatterns: ['node_modules', 'output'],
    coveragePathIgnorePatterns: ['node_modules', 'output'],

    moduleFileExtensions: ['vue', 'js', 'json', 'jsx', 'ts', 'tsx', 'node'],
};
