describe('Heroes: list', () => {
    it('should go to list on empty path', () => {
        cy.visit('/');
        cy.url().should('include', '/heroes');
    });

    it('should search by typing Adam', () => {
        cy.visit('/');
        cy.get('input[name=search]').type('adam');
        cy.get('.hero-item').should('have.length', 2);
    });

    it('should search by select first comics', () => {
        cy.visit('/');
        cy.get('select[name=comis]').select('Avengers: The Initiative (2007) #14');
        cy.get('.hero-item').should('have.length', 1);
    });

    it('should go to Hero view', () => {
        cy.visit('/');
        cy.get('.hero-item').eq(0).click();
        cy.url().should('include', '/heroes/1011334');
    });
});

describe('Heroes: view', () => {
    it('should see details', () => {
        cy.visit('/heroes/1011334');
        cy.get('.tabs .is-active + li a').click();
        cy.get('.tabs .is-active span').contains('3');
        cy.get('.table tr').should('have.length', 3);
    });
});
