describe('Navigation', () => {
    it('should go to teams', () => {
        cy.visit('/');
        cy.get('.navbar-start a:last-child').click();
        cy.url().should('include', '/teams');
        cy.get('h2').contains('Vos équipes');
    });

    it('should go back to home', () => {
        cy.visit('/teams');
        cy.get('.navbar-brand .navbar-item').click();

        cy.url().should('include', '/heroes');
    });
});
