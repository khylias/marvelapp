describe('Teams: create', () => {
    it('should go to Team Form', () => {
        cy.visit('/teams');

        cy.get('a[data-bot=team-create]').click();

        cy.url().should('include', '/teams/create');
        cy.get('h2').contains('Créer votre équipe');
    });

    it('should fail for heroes / name and save', () => {
        cy.visit('/teams/create');

        cy.get('.hero-item').eq(0).click();
        cy.get('p[data-bot=team-form-count]').contains('1/3');
        cy.get('.hero-item').eq(1).click();
        cy.get('p[data-bot=team-form-count]').contains('2/3');

        cy.get('button[data-bot=team-form-create]').click();
        cy.get('input[name=name]').should('have.class', 'is-danger');
        cy.get('p.is-danger').should('exist');
        cy.get('.notification.is-danger').should('exist');
    });

    it('should success create', () => {
        cy.visit('/teams/create');

        cy.get('input[name=search]').type('angel');
        cy.get('.hero-item').eq(0).click();
        cy.get('.hero-item').eq(1).click();
        cy.get('.hero-item').eq(2).click();
        cy.get('input[name=name]').type('My Team');

        cy.get('button[data-bot=team-form-create]').click();

        cy.url().should('include', '/teams');
        cy.get('h2').contains('Vos équipes');
        cy.get('.team-item p').contains('My Team');
    });
});
