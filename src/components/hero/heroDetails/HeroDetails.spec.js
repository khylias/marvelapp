import { shallowMount } from '@vue/test-utils';
import HeroDetails from './HeroDetails.vue';
describe('HeroDetails', () => {
    const props = {
        details: {
            available: 12,
            collectionURI: '',
            items: [
                {
                    ressourceURI: '/ressource/1',
                    name: 'Ressource 1',
                },
                {
                    ressourceURI: '/ressource/2',
                    name: 'Ressource 2',
                },
            ],
        },
    };
    it('should mount', () => {
        const wrapper = shallowMount(HeroDetails, {
            props,
        });

        expect(wrapper.exists()).toBeTruthy();
    });

    it('should display ressources', () => {
        const wrapper = shallowMount(HeroDetails, {
            props,
        });

        expect(wrapper.findAll('tr').length).toBe(2);
        expect(wrapper.findAll('td').at(0).text()).toBe('Ressource 1');
        expect(wrapper.findAll('td').at(1).text()).toBe('Ressource 2');
    });
});
