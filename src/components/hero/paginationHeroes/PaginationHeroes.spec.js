import { shallowMount } from '@vue/test-utils';
import PaginationHeroes from './PaginationHeroes.vue';

describe('PaginationHeroes', () => {
    const props = {
        pages: 20,
        modelValue: 1,
    };

    it('should mount', () => {
        const wrapper = shallowMount(PaginationHeroes, {
            props,
        });

        expect(wrapper.exists()).toBeTruthy();
    });

    it('should select first page', () => {
        const wrapper = shallowMount(PaginationHeroes, {
            props,
        });

        expect(wrapper.find('.pagination-link.is-current').text()).toBe('1');
    });

    it('should display first range of pages', () => {
        const wrapper = shallowMount(PaginationHeroes, {
            props,
        });

        expect(wrapper.findAll('.pagination-link').at(0).text()).toBe('1');
        expect(wrapper.findAll('.pagination-link').at(1).text()).toBe('2');
        expect(wrapper.findAll('.pagination-link').at(2).text()).toBe('3');
        expect(wrapper.find('.pagination-previous').classes('is-disabled')).toBeTruthy();
        expect(wrapper.find('.pagination-next').classes('is-disabled')).toBeFalsy();
    });

    it('should display last range of pages', () => {
        const wrapper = shallowMount(PaginationHeroes, {
            props: {
                ...props,
                modelValue: 20,
            },
        });

        expect(wrapper.findAll('.pagination-link').at(0).text()).toBe('1');
        expect(wrapper.findAll('.pagination-link').at(1).text()).toBe('18');
        expect(wrapper.findAll('.pagination-link').at(2).text()).toBe('19');
        expect(wrapper.findAll('.pagination-link').at(3).text()).toBe('20');
        expect(wrapper.find('.pagination-previous').classes('is-disabled')).toBeFalsy();
        expect(wrapper.find('.pagination-next').classes('is-disabled')).toBeTruthy();
    });

    it('should display middle range of pages', () => {
        const wrapper = shallowMount(PaginationHeroes, {
            props: {
                ...props,
                modelValue: 10,
            },
        });

        expect(wrapper.findAll('.pagination-link').at(0).text()).toBe('1');
        expect(wrapper.findAll('.pagination-link').at(1).text()).toBe('9');
        expect(wrapper.findAll('.pagination-link').at(2).text()).toBe('10');
        expect(wrapper.findAll('.pagination-link').at(3).text()).toBe('11');
        expect(wrapper.findAll('.pagination-link').at(4).text()).toBe('20');
        expect(wrapper.find('.pagination-previous').classes('is-disabled')).toBeFalsy();
        expect(wrapper.find('.pagination-next').classes('is-disabled')).toBeFalsy();
    });

    it('should go to next page', async () => {
        const wrapper = shallowMount(PaginationHeroes, {
            props: {
                ...props,
                modelValue: 10,
            },
        });

        await wrapper.find('.pagination-next').trigger('click');

        expect(wrapper.emitted('update:modelValue')).toHaveLength(1);
        expect(wrapper.emitted('update:modelValue')[0][0]).toEqual(11);
    });

    it('should go to next page', async () => {
        const wrapper = shallowMount(PaginationHeroes, {
            props: {
                ...props,
                modelValue: 10,
            },
        });

        await wrapper.find('.pagination-previous').trigger('click');

        expect(wrapper.emitted('update:modelValue')).toHaveLength(1);
        expect(wrapper.emitted('update:modelValue')[0][0]).toEqual(9);
    });

    it('should trigger set page', async () => {
        const wrapper = shallowMount(PaginationHeroes, {
            props: {
                ...props,
                modelValue: 10,
            },
        });

        wrapper.vm.setPage = jest.fn();

        await wrapper.find('.pagination-link').trigger('click');

        expect(wrapper.vm.setPage).toHaveBeenCalled();
    });

    it('should set page', async () => {
        const wrapper = shallowMount(PaginationHeroes, {
            props: {
                ...props,
                modelValue: 10,
            },
        });

        wrapper.vm.setPage(1);

        expect(wrapper.emitted('update:modelValue')).toHaveLength(1);
        expect(wrapper.emitted('update:modelValue')[0][0]).toEqual(1);
    });
});
