const { shallowMount } = require('@vue/test-utils');
import HeroItem from './HeroItem.vue';

describe('HeroItem', () => {
    const hero = {
        id: 1,
        name: 'HUMAN',
        thumbnail: {},
    };
    it('should mount', () => {
        const wrapper = shallowMount(HeroItem, {
            props: {
                hero,
            },
        });

        expect(wrapper.exists()).toBeTruthy();
        expect(wrapper.text()).toBe('HUMAN');
    });

    it('should be disabled', () => {
        const wrapper = shallowMount(HeroItem, {
            props: {
                hero,
                disabled: true,
            },
        });

        expect(wrapper.exists()).toBeTruthy();
        expect(wrapper.find('.hero-item').classes('hero-item--disabled')).toBeTruthy();
    });

    it('should be active', () => {
        const wrapper = shallowMount(HeroItem, {
            props: {
                hero,
                active: true,
            },
        });

        expect(wrapper.exists()).toBeTruthy();
        expect(wrapper.find('.hero-item').classes('hero-item--active')).toBeTruthy();
    });
});
