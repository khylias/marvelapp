const { shallowMount } = require('@vue/test-utils');
import HeroLink from './HeroLink.vue';

describe('HeroItem', () => {
    const hero = {
        id: 1,
        name: 'HUMAN',
    };
    it('should mount', () => {
        const wrapper = shallowMount(HeroLink, {
            props: {
                hero,
            },
        });

        expect(wrapper.exists()).toBeTruthy();
    });

    it('should have hero id', () => {
        const wrapper = shallowMount(HeroLink, {
            props: {
                hero,
            },
        });

        expect(wrapper.find('router-link-stub').attributes('to')).toBe('/heroes/1');
    });
});
