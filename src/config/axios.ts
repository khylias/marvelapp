import axios from 'axios';
import md5 from 'md5';

const ts = new Date().getTime();
const publicKey = import.meta.env.VITE_MARVEL_PUBLIC_KEY;
const privateKey = import.meta.env.VITE_MARVEL_PRIVATE_KEY;

export default axios.create({
    baseURL: 'https://gateway.marvel.com:443/v1/public',
    timeout: 10000,
    params: {
        ts,
        apikey: publicKey,
        hash: md5(ts + privateKey + publicKey),
    },
});
