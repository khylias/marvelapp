import { computed, ref } from 'vue';

export default function usePagination() {
    const currentPage = ref<number>(1);
    const total = ref<number>(0);
    const limit = ref<number>(50);
    const count = ref<number>(0);
    const offset = computed(() => (currentPage.value - 1) * limit.value);
    const pages = computed(() => Math.ceil(total.value / limit.value));

    return {
        offset,
        currentPage,
        total,
        limit,
        count,
        pages,
    };
}
