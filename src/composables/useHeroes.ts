import axios from '@/config/axios';
import type { Hero } from '@/models/hero';
import { ref, watch } from 'vue';
import usePagination from './usePagination';

export default function useHeroes() {
    const heroes = ref<Hero[]>([]);
    const hero = ref<null | Hero>();
    const loading = ref<boolean>(false);
    const { limit, offset, currentPage, pages, total } = usePagination();

    async function getAll(params?: object) {
        loading.value = true;
        const response = await axios.get('/characters', {
            params: {
                ...params,
                limit: limit.value,
                offset: offset.value,
            },
        });
        heroes.value = response.data.data.results;
        total.value = response.data.data.total;
        loading.value = false;
    }

    async function getOne(id: number, params?: object) {
        loading.value = true;
        const response = await axios.get(`/characters/${id}`, { params });
        hero.value = response.data.data.results[0];
        loading.value = false;
    }

    watch(offset, () => {
        getAll();
    });

    return {
        heroes,
        hero,
        loading,
        limit,
        offset,
        currentPage,
        pages,
        total,
        getAll,
        getOne,
    };
}
