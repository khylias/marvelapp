import { createRouter, createWebHistory } from 'vue-router';

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            redirect: '/heroes',
        },
        {
            path: '/heroes',
            name: 'heroes',
            children: [
                {
                    path: '',
                    name: 'heroes.all',
                    component: () => import('@/views/HeroesView.vue'),
                },
                {
                    path: ':id',
                    name: 'heroes.single',
                    component: () => import('@/views/HeroView.vue'),
                },
            ],
        },
        {
            path: '/teams',
            name: 'teams',
            children: [
                {
                    path: '',
                    name: 'teams.all',
                    component: () => import('@/views/TeamsView.vue'),
                },
                {
                    path: 'create',
                    name: 'teams.create',
                    component: () => import('@/views/TeamFormView.vue'),
                },
                {
                    path: ':id',
                    name: 'teams.single',
                    component: () => import('@/views/TeamView.vue'),
                },
            ],
        },
        {
            path: '/:pathMatch(.*)',
            name: 'notfound',
            component: () => import('@/views/NotFound.vue'),
        },
    ],
});

export default router;
