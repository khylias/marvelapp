import type { Team } from '@/models/team';
import { defineStore } from 'pinia';
import { reactive } from 'vue';

export const useTeamsStore = defineStore('teams', () => {
    const teams = reactive<Team[]>([]);

    function create(team: Team) {
        teams.push({
            ...team,
            id: Math.floor(Math.random() * 1000),
        });
    }

    function update(team: Team) {
        const index = teams.findIndex((item) => item.id === team.id);
        const updatedTeam = {
            ...teams.find((item) => item.id === team.id),
            ...team,
        };

        teams.splice(index, 1, updatedTeam);
    }

    function getTeam(id: number): Team | undefined {
        return teams.find((team: Team) => team.id === id);
    }

    return {
        teams,
        create,
        update,
        getTeam,
    };
});
