import type { Hero } from '@/models/hero';
import useHeroes from '@/composables/useHeroes';
import { defineStore } from 'pinia';
import { computed, ref } from 'vue';
import { useTeamsStore } from './teams';

export const useHeroesStore = defineStore('heroes', () => {
    const teamStore = useTeamsStore();
    const { heroes, hero, getAll, getOne, loading, currentPage, pages, limit } = useHeroes();
    const resultsHeroes = ref<Hero[]>([]);

    const availablesHeroes = computed<Hero[]>(() => {
        const heroesFiltered = heroes.value.filter((hero: Hero) => {
            let heroesByTeam: Hero[] = [];

            teamStore.teams.forEach((team) => {
                heroesByTeam = [...heroesByTeam, ...team.heroes];
            });
            return heroesByTeam.findIndex((teamedHero) => teamedHero.id === hero.id) === -1;
        });
        return heroesFiltered;
    });

    const availablesComics = computed<any>(() => {
        return new Set(
            resultsHeroes.value
                .filter((hero) => 'comics' in hero)
                .map((hero) => hero.comics?.items.map((item) => item.name))
                .flat(),
        );
    });

    function filterHeroes(
        filter: {
            search: string;
            comics: string;
        },
        onlyAvailableHeroes: boolean,
    ) {
        let items = onlyAvailableHeroes ? availablesHeroes.value : heroes.value;

        if (filter.search.length) {
            const searchKeys = filter.search.toLowerCase();
            items = items.filter(
                (hero: Hero) =>
                    hero.name.toLowerCase().includes(searchKeys) ||
                    hero.description?.toLowerCase().includes(searchKeys),
            );
        }

        if (filter.comics.length) {
            items = items.filter((hero) =>
                hero?.comics?.items.map((comic) => comic.name).includes(filter.comics),
            );
        }

        resultsHeroes.value = items;
    }

    function fetchHeroes() {
        getAll();
    }
    function fetchHero(id: number) {
        getOne(id);
    }

    return {
        heroes,
        hero,
        currentPage,
        pages,
        loading,
        limit,
        availablesHeroes,
        availablesComics,
        resultsHeroes,
        filterHeroes,
        fetchHeroes,
        fetchHero,
    };
});
