import type { AbstractRessource } from './abstract-ressource';

export interface Events {
    available: number;
    collectionURI: string;
    items: Event[];
}

export interface Event extends AbstractRessource {}
