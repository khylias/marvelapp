import type { AbstractRessource } from './abstract-ressource';

export interface Comics {
    available: number;
    collectionURI: string;
    items: Comic[];
}

export interface Comic extends AbstractRessource {}
