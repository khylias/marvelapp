import type { Events } from './event';
import type { Comics } from './comic';
import type { Series } from './serie';
import type { Stories } from './story';

export type HeroRessource = Events | Comics | Series | Stories;

export interface Hero {
    id: number;
    name: string;
    description?: string;
    thumbnail?: {
        path: string;
        extension: string;
    };
    comics?: Comics;
    series?: Series;
    events?: Events;
    stories?: Stories;
}
