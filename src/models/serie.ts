import type { AbstractRessource } from './abstract-ressource';

export interface Series {
    available: number;
    collectionURI: string;
    items: Serie[];
}

export interface Serie extends AbstractRessource {}
