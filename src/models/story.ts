import type { AbstractRessource } from './abstract-ressource';

export interface Stories {
    available: number;
    collectionURI: string;
    items: Story[];
}

export interface Story extends AbstractRessource {
    type: string;
}
